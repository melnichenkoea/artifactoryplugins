import groovy.transform.Field
import org.artifactory.repo.RepoPath
import org.artifactory.search.aql.AqlResult
import org.artifactory.repo.RepoPathFactory

//   IMPORTANT NOTE:
//   I tried many times and couldn't think of how to separate the "main version" if the separating character changes every time. And the name pattern changes too
//   Now the plugin can work with the following patterns  versions:
//   - 7.4.0-187
//   - 7.3.0-1338
//   - 1.1.2
//   - 2.2
//   - 3.4-111
//   And can't work with:
//   - 7.4.8-local
//   - 7.5.6-111katya
//   - apsPackageVersion
//   - 7.3.0-510_50f2a5_19812
//   - 1.2.0-SNAPSHOT
//   - disable_test/1.0-2
//   - 1.1.2.upgrade
//
//   If any have some idea, pls, comment in git


// Example REST API calls:
// curl -X POST -v -u admin:<SET_PASSWORD> "http://localhost:8050/artifactory/api/plugins/execute/dockerImagesCleanup?params=repo=cloud;paths=earm-war,export-engine"

// Name of config file must equal name of plugin
@Field final String PROPERTIES_FILE_PATH = "plugins/${this.class.name}.properties"
def config = new ConfigSlurper().parse(new File(ctx.artifactoryHome.haAwareEtcDir, PROPERTIES_FILE_PATH).toURL())

executions {
    dockerImagesCleanup(version: '1.1', description: 'Deletes excess docker images', users: ['admin'].toSet()) { params ->
        if (!params || !params.repo && !params.paths) {
            def errorMessage = 'Repo and paths parameter is mandatory, please supply it.'
            log.error errorMessage
            status = 400
            message = errorMessage
        } else {
             repo = params?.get('repo')?.get(0) as String
             removeExcessList(repo, params.paths as String[])
        }
    }
}

// Set default value if config file empty
log.info "Schedule job policy list: $config.policies"
config.policies.each{ policySettings ->
    def cron = policySettings[ 0 ] ? policySettings[ 0 ] as String : ["0 0 5 ? * 1"]
    def repo = policySettings[ 1 ] ? policySettings[ 1 ] as String : ["__none__"]
    def paths = policySettings[ 2 ] ? policySettings[ 2 ] as String[] : ["all"]

    // It's entrypoint for execute plugin like cron job
    // In config file set time execute, repository and paths. Paths may be several and separate by comma.
    jobs {
        "scheduled dockerImagesCleanup_$cron"(cron: cron) {
            log.info "Policy settings for scheduled run at($cron): paths list($paths)"
            removeExcessList( repo, paths )
        }
    }
}

// Function take on list of path and call remove function for every item of list
private def removeExcessList(String repo, String[] paths){
    if (paths.size() == 1 && paths.contains("all")){
        aql = 'items.find({"repo":\"' + repo + '\"})'
        searches.aql(aql) { AqlResult result ->
            log.info("Clear ALL  directory in $repo repository")
            def repositoryList = []
            result.each { Map item ->
                tmpPath = item.path
                itemPath = tmpPath.substring(0, tmpPath.lastIndexOf('/'))
                repositoryList << itemPath
                repositoryList.unique()
            }
            repositoryList.each {
                log.info "Clear directory: $it"
                removeExcess(repo, it)
            }
        }
    } else {
        paths.each {
            log.info "Clear directory: $it"
            removeExcess(repo, it)
        }
    }
}

def removeExcess(String repo, String path) {
    log.info "Count items in $repo/$path"
    def numberRegexp = /^(([0-9]+)(\.[0-9]+)?(\.[0-9]+)?(\.[0-9]+))?(-[0-9]+)?$/;

    def fullVersionList = [];
    def majorVersionList = [];

    // Request to artifactory: get all item in "repo" with name who match with "path" and exclude "latest" in name
    //def aql = 'items.find({"repo":\"' + repo + '\"}, {"path":{"$match":\"*' + path + '*\"}}, {"path":{"$nmatch":"*_uploads"}}, {"path":{"$nmatch":"*7.5.0.local"}})'
    def aql = 'items.find({"repo":\"' + repo + '\"}, {"path":{"$match":\"*' + path + '*\"}})'
    searches.aql(aql) { AqlResult result ->
        result.each { Map item ->
            def tmpPath = item.path
            if (tmpPath.substring(tmpPath.lastIndexOf('/') + 1) ==~ numberRegexp){
                def tmpVersion = tmpPath.substring(tmpPath.lastIndexOf('/') + 1) =~ numberRegexp;
                def version = tmpVersion[0][0];
                 
                // Save full number version to list, it's required for find major version of image
                fullVersionList << version;
                fullVersionList.unique()

                // Desc sort
                fullVersionList.sort({ a, b ->
                    def (w1, x1, y1, z1) = a.split("\\.|-").collect { it.toInteger() }
                    def (w2, x2, y2, z2) = b.split("\\.|-").collect { it.toInteger() }
                    w2 <=> w1 ?: x2 <=> x1 ?: y2 <=> y1 ?: z2 <=>z1
                })
                // Major version it's all number before last separate symbol
                fullVersionList.each{
                    majorVersionList << (it.substring(0, it.lastIndexOf(".") + 1))
                    majorVersionList.unique();
                }
            }
        }

        // For every major version create list with full path - listToRemove
        majorVersionList.each { major ->
            log.info "Major version: $major"
            def listToRemove = [];
            fullVersionList.each { version ->
                if (version.contains(major)){
                    listToRemove << (path + "/" + version)
                }
            }

            // If listToRemove size more 50 item
            if (listToRemove.size > 50){
                def count = 0
                // Offset first 100 element and remove next
                listToRemove[50 .. -1].each{
                    RepoPath repoPath = RepoPathFactory.create(repo, it)
                    log.info "Deleting: $repoPath"
                    //repositories.delete repoPath
                    count++
                }
                log.info ("Succesfully deleted  " + count + " files")
            } else {
                log.info ("Count less 50. Did not delete anything")
                status = 200
            }
        }
    }
}
